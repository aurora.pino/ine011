# Visualizador Problema de la Mochila 0/1 multidimensional (MKP)

Implementado en javascript. Para ejecutarlo, [instalar node](https://nodejs.org/).

## Puesta en marcha

En un terminal, ir a la carpeta en donde estan los archivos y realizar lo siguiente.

1) Instalar dependencias:
```
npm install
```

2) Ejecutar servidor para dejar aplicación en marcha:
```
npm run serve
```

3) Ir a [http://localhost:8080/] en un navegador.

## Elementos relevante del problema

El componente [CargadorInstancia.vue](src/components/CargadorInstancia.vue) contiene la estructura de datos y lógica para cargar un dataset específico. La función `cargar()` se ejecuta para obtener el contenido de la entrada de texto y procesarlo, guardando los datos de la instancia en la variable `datosInstancia`:
```javascript
function cargar() {
    let res = {
        items: [],
        mochilas: [],
        restricciones: []
    };

    if(inputTexto.value) {
        let lines = inputTexto.value.trim().split("\n");            
        // Se realiza todo el procesamiento línea por línea        
    }
    
    datosInstancia.value = res;
}
```

Luego, el contenido de la estructura `datosInstancia` se pasa al componente [VisorInstancia.vue](src/components/VisorInstancia.vue) para generar la tabla de visualización. Todo se aplica a través de la plantilla del componente:
```html
<script setup>
    const props = defineProps(['datosInstancia']);
</script>

<template>
    <div class="visorInstancia">
        <!-- -->
                <div class="items">                    
                    <div v-for="item in props.datosInstancia.items" :key="item">
                        {{ item }}
                    </div>
                </div>
        <!-- -->        
                <div class="mochilas">                    
                    <div v-for="item in props.datosInstancia.mochilas" :key="item">
                        {{ item }}
                    </div>
                </div>
        <!-- -->
                <div class="restrRow" v-for="i in props.datosInstancia.restricciones.length" :key="i">
                    <div class="restricciones" v-for="item in props.datosInstancia.restricciones[i - 1]" :key="item">
                            {{ item }}
                    </div>
                </div>
        <!-- -->
    </div>
</template>
```